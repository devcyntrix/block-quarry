package de.devcyntrix.quarry;

import de.devcyntrix.quarry.utils.Blocks;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class BlockQuarry implements Listener, AutoCloseable {

    private final BlockQuarryPlugin plugin;
    private final BlockQuarryService service;
    @Getter
    private Block quarryBlock;
    private final Predicate<Block> mineBlock;
    private final Chunk[] chunks;

    private AtomicLong predictedBlocks = new AtomicLong(0);
    private AtomicLong brokeBlocks = new AtomicLong(0);

    @Getter
    private volatile Future<?> currentTask;

    public BlockQuarry(BlockQuarryService service, Block quarryBlock, Predicate<Block> mineBlock, Chunk... chunks) {
        this.plugin = service.getPlugin();
        this.service = service;
        this.quarryBlock = quarryBlock;
        this.mineBlock = mineBlock;
        this.chunks = chunks;

        // Registers listener
        Bukkit.getPluginManager().registerEvents(this, this.plugin);
    }

    public @Nullable CompletableFuture<Long> calculateBlocks(boolean interruptRunningTask, long sleepTime, Predicate<Block> sleep) {
        // Check for running task
        if (this.currentTask != null) {
            if (!interruptRunningTask && !this.currentTask.isDone())
                return null;
            this.currentTask.cancel(true);
        }

        // Reset
        this.predictedBlocks.set(0);

        if (chunks.length == 0)
            return CompletableFuture.completedFuture(0L);

        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            try {
                for (Chunk chunk : chunks) {
                    for (int y = quarryBlock.getY() - 1; y >= 0; y--) {
                        for (int x = 0; x < 16; x++) {
                            for (int z = 0; z < 16; z++) {
                                Block block = chunk.getBlock(x, y, z);

                                if (sleep.test(block))
                                    Thread.sleep(sleepTime);

                                if (mineBlock.test(block)) {
                                    this.predictedBlocks.incrementAndGet();
                                }
                            }
                        }
                    }
                }
            } catch (InterruptedException ignored) { }
            return this.predictedBlocks.longValue();
        }, this.service.getExecutorService());
        this.currentTask = future;
        return future;
    }

    public @Nullable CompletableFuture<Long> startMining(boolean interruptRunningTask, long sleepTime, Predicate<Block> sleep, Consumer<ItemStack> itemStackConsumer) {
        // Check for running task
        if (this.currentTask != null) {
            System.out.println(this.currentTask);
            if (!interruptRunningTask && !this.currentTask.isDone()) {
                Bukkit.broadcastMessage("Isn't done");
                return null;
            }
            this.currentTask.cancel(true);
        }

        if (chunks.length == 0)
            return CompletableFuture.completedFuture(0L);


        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {

            try {
                for (Chunk chunk : chunks) {
                    for (int y = quarryBlock.getY() - 1; y >= 0; y--) {
                        for (int x = 0; x < 16; x++) {
                            for (int z = 0; z < 16; z++) {
                                Block block = chunk.getBlock(x, y, z);

                                if (sleep.test(block))
                                    Thread.sleep(sleepTime);

                                if (mineBlock.test(block)) {
                                    Bukkit.getScheduler().runTask(this.plugin, () -> {
                                        Collection<ItemStack> drops = block.getDrops();
                                        for (ItemStack drop : drops) {
                                            itemStackConsumer.accept(drop);
                                        }
                                        block.setType(Material.STONE);
                                    });

                                    brokeBlocks.incrementAndGet();
                                }
                            }
                        }
                    }
                }
            } catch (InterruptedException ignored) {}
            return this.brokeBlocks.longValue();
        }, this.service.getExecutorService());

        this.currentTask = future;
        return future;
    }

    public void interruptTask() {
        // Interrupts task
        if (this.currentTask != null) {
            this.currentTask.cancel(true);
            Bukkit.broadcastMessage("Cancelled");
        }
    }

    @ApiStatus.Internal
    @EventHandler
    public final void onBlockBreak(BlockBreakEvent event) {
        Bukkit.broadcastMessage("HANDLE");
        if (!Blocks.isSameBlock(event.getBlock(), this.quarryBlock))
            return;
        service.deleteBlockQuarry(this);
        Bukkit.broadcastMessage("Block broke");
    }

    @ApiStatus.Internal
    @EventHandler
    public final void onDisablePlugin(PluginDisableEvent event) {
        if (event.getPlugin() != this.plugin)
            return;

        Bukkit.broadcastMessage("Interrupting async task...");
        try {
            close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void close() throws Exception {
        interruptTask();
        HandlerList.unregisterAll(this);
    }

    public static enum State {
        CALCULATE,
        RUNNING,
        DONE
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockQuarry quarry = (BlockQuarry) o;
        return Objects.equals(quarryBlock, quarry.quarryBlock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quarryBlock);
    }
}
