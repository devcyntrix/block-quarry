package de.devcyntrix.quarry;

import de.devcyntrix.quarry.listeners.QuarrySetupListener;
import org.bukkit.plugin.java.JavaPlugin;

public class BlockQuarryPlugin extends JavaPlugin {

    private BlockQuarryService blockQuarryService;

    @Override
    public void onDisable() {
        try {
            this.blockQuarryService.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onEnable() {
        this.blockQuarryService = new BlockQuarryService(this);

        getServer().getPluginManager().registerEvents(new QuarrySetupListener(this, this.blockQuarryService), this);
    }

    public BlockQuarryService getBlockQuarryService() {
        return blockQuarryService;
    }
}
