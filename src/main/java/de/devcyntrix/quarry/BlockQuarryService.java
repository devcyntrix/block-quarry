package de.devcyntrix.quarry;

import lombok.Getter;
import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;

public class BlockQuarryService implements AutoCloseable {

    @Getter
    private final BlockQuarryPlugin plugin;
    private final Set<BlockQuarry> quarries = new CopyOnWriteArraySet<>();
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public BlockQuarryService(BlockQuarryPlugin plugin) {
        this.plugin = plugin;
    }

    public @Nullable BlockQuarry createBlockQuarry(Block block, Predicate<Block> mineBlock, Chunk... chunks) {
        BlockQuarry quarry = new BlockQuarry(this, block, mineBlock, chunks);
        boolean add = this.quarries.add(quarry);
        if (!add)
            return null;
        return quarry;
    }

    public @Nullable BlockQuarry getQuarry(@NotNull Block block) {
        return this.quarries.stream().filter(quarry -> quarry.getQuarryBlock() == block).findFirst().orElse(null);
    }

    public boolean deleteBlockQuarry(BlockQuarry quarry) {
        try {
            quarry.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return this.quarries.remove(quarry);
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    @Override
    public void close() throws Exception {
        quarries.forEach(BlockQuarry::interruptTask);

        this.executorService.shutdown();
        // TODO: Save the quarries
    }
}
