package de.devcyntrix.quarry;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.jetbrains.annotations.Nullable;

@Data
public class Position {

    @SerializedName("world")
    private String worldName;

    private double x, y, z;
    private float yaw, pitch;

    public @Nullable Location toLocation() {
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            return null;
        }
        return new Location(world, x, y, z, yaw, pitch);
    }

}
