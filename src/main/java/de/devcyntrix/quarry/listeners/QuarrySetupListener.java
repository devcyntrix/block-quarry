package de.devcyntrix.quarry.listeners;

import de.devcyntrix.quarry.BlockQuarry;
import de.devcyntrix.quarry.BlockQuarryPlugin;
import de.devcyntrix.quarry.BlockQuarryService;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

public class QuarrySetupListener implements Listener {

    private final BlockQuarryPlugin plugin;
    private final BlockQuarryService service;

    public QuarrySetupListener(BlockQuarryPlugin plugin, BlockQuarryService service) {
        this.plugin = plugin;
        this.service = service;
    }

    @EventHandler
    public void onQuarryPlace(BlockPlaceEvent event) {
        if (event.getBlock().getType() != Material.CAULDRON)
            return;

        Predicate<Block> breakBlock = block -> block.getType() == Material.IRON_ORE;

        Player player = event.getPlayer();
        player.sendMessage("Quarry placed");
        BlockQuarry quarry = this.service.createBlockQuarry(event.getBlock(), breakBlock, event.getBlock().getChunk());

        if (quarry == null) {
            player.sendMessage("§cFehler beim erstellen der Quarry");
            return;
        }

        // Will never sleep
        @Nullable CompletableFuture<Long> b = quarry.calculateBlocks(false, 0, block -> true);
        if (b == null) {
            player.sendMessage("§cEs wird schon eine Aufgabe ausgeführt.");
            return;
        }

        this.service.getExecutorService().execute(() -> {
            try {
                Long predictedBlockCount = b.get();
                while (!quarry.getCurrentTask().isDone()) ;
                player.sendMessage("§7Es wurden §a" + predictedBlockCount + " Blöcke §7in den Chunks gefunden.");
                CompletableFuture<Long> future = quarry.startMining(false, 1000 / 50, block -> false, itemStack -> {
                    Location location = event.getBlock().getLocation();
                    location.getWorld().dropItem(location.add(0.5, 1, 0.5), itemStack)
                            .setVelocity(new Vector(0, 0, 0));
                });
                Long minedBlockCount = future.get();
                while (!quarry.getCurrentTask().isDone()) ;
                player.sendMessage("§7Es wurden ingesamt §a" + minedBlockCount + "/" + predictedBlockCount + " Blöcke §7abgebaut.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
                player.sendMessage("§cEs ist ein Fehler ausgetreten beim Starten des Abbauprozesses");
            }
        });

    }

}
