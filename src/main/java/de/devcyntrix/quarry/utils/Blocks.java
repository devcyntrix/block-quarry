package de.devcyntrix.quarry.utils;

import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;

public final class Blocks {

    public static boolean isSameBlock(@NotNull Block block, @NotNull Block block1) {
        return block.getType() == block1.getType() && block.getLocation().equals(block1.getLocation());
    }

}
